//
//  AppDelegate.swift
//  20220630-SeanMurphy-NYCSchools
//
//  Created by Sean Murphy on 6/30/22.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {


    var window: UIWindow?
    
    var highSchoolVC = HighSchoolVC()
    var detailsVC = DetailsVC()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        window?.backgroundColor = .systemBackground
        window?.rootViewController = HighSchoolVC()
        
        return true
    }
    
    private func displayNextScreen() {
        setRootViewController(detailsVC)
    }


}

extension AppDelegate {
    func setRootViewController(_ vc: UIViewController, animated: Bool = true) {
        guard animated, let window = self.window else {
            self.window?.rootViewController = vc
            self.window?.makeKeyAndVisible()
            return
        }

        window.rootViewController = vc
        window.makeKeyAndVisible()
        UIView.transition(with: window,
            duration: 0.3,
            options: .transitionCrossDissolve,
            animations: nil,
            completion: nil)
    }
}

extension AppDelegate: HighSchoolCellDelegate {
    func didShowDetailsView() {
        displayNextScreen()
    }

}

