//
//  CombinedModel.swift
//  20220630-SeanMurphy-NYCSchools
//
//  Created by Sean Murphy on 7/1/22.
//

import Foundation

struct CombinedModel: Codable {
    let school_name: String
    let overview_paragraph: String
    let num_of_sat_test_takers: String
    let sat_critical_reading_avg_score: String
    let sat_math_avg_score: String
    let sat_writing_avg_score: String
}
