//
//  SchoolModel.swift
//  20220630-SeanMurphy-NYCSchools
//
//  Created by Sean Murphy on 6/30/22.
//

import Foundation

struct HighSchoolModel: Codable {
    let school_name: String
    let overview_paragraph: String
}
