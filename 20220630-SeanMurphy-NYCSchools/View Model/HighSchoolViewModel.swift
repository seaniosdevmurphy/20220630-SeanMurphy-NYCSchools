//
//  HighSchoolViewModel.swift
//  20220630-SeanMurphy-NYCSchools
//
//  Created by Sean Murphy on 6/30/22.
//

import Foundation

class HighSchoolListsViewModel {
    
    var highSchoolsViewModel: [HighSchoolViewModel]
    
    init() {
        self.highSchoolsViewModel = [HighSchoolViewModel]()
    }
}

extension HighSchoolListsViewModel {
    func highSchoolViewModel(at index: Int) -> HighSchoolViewModel {
        return self.highSchoolsViewModel[index]
    }
}

struct HighSchoolViewModel {
    
    let highSchool: HighSchoolModel
    
    var name: String {
        return self.highSchool.school_name
    }
    
    var detail: String {
        return self.highSchool.overview_paragraph
    }
}
