//
//  ScoreViewModel.swift
//  20220630-SeanMurphy-NYCSchools
//
//  Created by Sean Murphy on 7/1/22.
//

import Foundation

class ScoreListViewModel {
    
    var scores: [ScoreViewModel]
    
    init() {
        self.scores = [ScoreViewModel]()
    }
}

extension ScoreListViewModel {
    func scoreViewModel(at index: Int) -> ScoreViewModel {
        return self.scores[index]
    }
}

struct ScoreViewModel {
    let scoreModel: ScoresModel
    
    var mathScore: String {
        return self.scoreModel.sat_math_avg_score
    }
    
    var readingScore: String {
        return self.scoreModel.sat_critical_reading_avg_score
    }
    
    var totalTests: String {
        return self.scoreModel.num_of_sat_test_takers
    }
    
    var writingScore: String {
        return self.scoreModel.sat_writing_avg_score
    }
    
    
}
