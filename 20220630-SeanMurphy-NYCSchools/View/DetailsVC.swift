//
//  DetailsVC.swift
//  20220630-SeanMurphy-NYCSchools
//
//  Created by Sean Murphy on 7/1/22.
//

import UIKit

class DetailsVC: UIViewController {
    let mathLabel = UILabel()
    let writingLabel = UILabel()
    let readingLabel = UILabel()
    let totalTestsLabel = UILabel()
    let stackView = UIStackView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        style()
        layout()
    }
}

extension DetailsVC {
    func style() {
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 20
        
        mathLabel.translatesAutoresizingMaskIntoConstraints = false
        mathLabel.font = UIFont.preferredFont(forTextStyle: .title3)
        mathLabel.adjustsFontForContentSizeCategory = true
        mathLabel.text = "School Name"
        
        writingLabel.translatesAutoresizingMaskIntoConstraints = false
        writingLabel.font = UIFont.preferredFont(forTextStyle: .footnote)
        writingLabel.adjustsFontForContentSizeCategory = true
        writingLabel.text = "Details for the school go here"
    }
    
    func layout() {
        stackView.addArrangedSubview(totalTestsLabel)
        stackView.addArrangedSubview(mathLabel)
        stackView.addArrangedSubview(writingLabel)
        stackView.addArrangedSubview(readingLabel)
        
        view.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalToSystemSpacingBelow: view.topAnchor, multiplier: 2),
            stackView.leadingAnchor.constraint(equalToSystemSpacingAfter: view.leadingAnchor, multiplier: 2),
            stackView.bottomAnchor.constraint(equalToSystemSpacingBelow: view.bottomAnchor, multiplier: 2),
            stackView.trailingAnchor.constraint(equalToSystemSpacingAfter: view.trailingAnchor, multiplier: 2)
        ])

    }
}
