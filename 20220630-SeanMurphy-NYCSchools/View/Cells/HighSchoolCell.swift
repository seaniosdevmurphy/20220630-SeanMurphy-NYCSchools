//
//  HighSchoolCell.swift
//  20220630-SeanMurphy-NYCSchools
//
//  Created by Sean Murphy on 6/30/22.
//

import UIKit

protocol HighSchoolCellDelegate: AnyObject {
    func didShowDetailsView()
}

protocol DetailsDelegate: AnyObject {
    func fetchScores()
}

class HighSchoolCell: UITableViewCell {
    
    let schoolNameLabel = UILabel()
    let overviewLabel = UILabel()
    let button = UIButton(type: .system)
    let stackView = UIStackView()
    
    weak var delegate: HighSchoolCellDelegate?
    weak var detailsDelegate: DetailsDelegate?
    
    static let reuseID = "HighSchoolCell"
    static let rowHeight: CGFloat = 180
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder) has not been implemented")
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: 200, height: 200)
    }
}

extension HighSchoolCell {
    func setup() {
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 10
        
        schoolNameLabel.translatesAutoresizingMaskIntoConstraints = false
        schoolNameLabel.font = UIFont.preferredFont(forTextStyle: .title3)
        schoolNameLabel.adjustsFontForContentSizeCategory = true
        schoolNameLabel.text = "School Name"
        
        overviewLabel.translatesAutoresizingMaskIntoConstraints = false
        overviewLabel.font = UIFont.preferredFont(forTextStyle: .footnote)
        overviewLabel.adjustsFontForContentSizeCategory = true
        overviewLabel.numberOfLines = 2
        overviewLabel.text = "Details for the school go here"
        
        button.translatesAutoresizingMaskIntoConstraints = false
        button.configuration = .filled()
        button.setTitle("Show Details", for: [])
        button.addTarget(self, action: #selector(detailButtonTapped), for: .primaryActionTriggered)
    }
    
    func layout() {
        stackView.addArrangedSubview(schoolNameLabel)
        stackView.addArrangedSubview(overviewLabel)
        stackView.addArrangedSubview(button)
        
        contentView.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalToSystemSpacingBelow: topAnchor, multiplier: 1),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 16),
            stackView.leadingAnchor.constraint(equalToSystemSpacingAfter: leadingAnchor, multiplier: 2),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 8)

        ])

        
//        NSLayoutConstraint.activate([
//            schoolNameLabel.topAnchor.constraint(equalToSystemSpacingBelow: topAnchor, multiplier: 2),
//            schoolNameLabel.leadingAnchor.constraint(equalToSystemSpacingAfter: leadingAnchor, multiplier: 2),
//            schoolNameLabel.trailingAnchor.constraint(equalToSystemSpacingAfter: trailingAnchor, multiplier: 2)
//        ])
//
//        NSLayoutConstraint.activate([
//            overviewLabel.topAnchor.constraint(equalToSystemSpacingBelow: schoolNameLabel.bottomAnchor, multiplier: 1),
//            overviewLabel.leadingAnchor.constraint(equalToSystemSpacingAfter: leadingAnchor, multiplier: 2),
//            overviewLabel.trailingAnchor.constraint(equalToSystemSpacingAfter: trailingAnchor, multiplier: 4),
//            overviewLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 8),
//        ])


    }
    
    @objc func detailButtonTapped(_ sender: UIButton) {
        detailsDelegate?.fetchScores()
        delegate?.didShowDetailsView()
    }
}

