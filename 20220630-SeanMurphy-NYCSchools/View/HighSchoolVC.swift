//
//  HighSchoolVC.swift
//  20220630-SeanMurphy-NYCSchools
//
//  Created by Sean Murphy on 6/30/22.
//

import UIKit

protocol HighSchoolVCDelegate {
    func detailsButtonTapped()
}
class HighSchoolVC: UIViewController {
    
    var tableView = UITableView()
    var highSchoolListViewModel = HighSchoolListsViewModel()
    var scoreListViewModel = ScoreListViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

}

extension HighSchoolVC {
    private func setup() {
        setupTableView()
        loadSchools()
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(HighSchoolCell.self, forCellReuseIdentifier: HighSchoolCell.reuseID)
        tableView.rowHeight = HighSchoolCell.rowHeight
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
        ])

    }
    
    private func loadSchools() {

        guard let schoolURL = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json") else {
        fatalError("wrong url")
    }
        let resource = Resource<[HighSchoolModel]>(url: schoolURL)

        Webservice().load(resource: resource) { [weak self] result in
            switch result {
            case .success(let highSchool):
                self?.highSchoolListViewModel.highSchoolsViewModel = highSchool.map(HighSchoolViewModel.init)
                self?.tableView.reloadData()
            case .failure(let error):
                print(error)
            }
        }
    }
}

extension HighSchoolVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let vm = highSchoolListViewModel.highSchoolViewModel(at: indexPath.row)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: HighSchoolCell.reuseID, for: indexPath) as! HighSchoolCell
        
        cell.schoolNameLabel.text = vm.name
        cell.overviewLabel.text = vm.detail
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.highSchoolListViewModel.highSchoolsViewModel.count
//        return 1
    }
}

extension HighSchoolVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension HighSchoolVC: DetailsDelegate {
    func fetchScores() {
        guard let scoresURL = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json") else {
            fatalError("Failed fetching scores data")
        }
        let scoreResource = Resource<[ScoresModel]>(url: scoresURL)
        
        Webservice().load(resource: scoreResource) { [weak self] result in
            switch result {
            case .success(let scores):
                self?.scoreListViewModel.scores = scores.map(ScoreViewModel.init)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func showDetailsForSchool() {
        
    }
}
