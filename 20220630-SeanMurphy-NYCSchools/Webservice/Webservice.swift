//
//  Webservice.swift
//  20220630-SeanMurphy-NYCSchools
//
//  Created by Sean Murphy on 6/30/22.
//

import Foundation

enum NetworkError: Error {
    case domainError
    case decodingError
    case urlError
}

struct Resource<T: Codable> {
    let url: URL
}


class Webservice {
    
    func load<T>(resource: Resource<T>, completion: @escaping (Result<T, NetworkError>) -> Void) {

        URLSession.shared.dataTask(with: resource.url) { data, response, error in
            guard let data = data, error == nil else {
                completion(.failure(.domainError))
                return
            }
            
            let result = self.parseData(data)
            
            if let result = result {
                DispatchQueue.main.async {
                    completion(.success(result as! T))
                }
            } else {
                completion(.failure(.decodingError))
            }

        }.resume()
    }
    
    func parseData(_ data: Data) -> [HighSchoolModel]? {
        let result = try? JSONDecoder().decode([HighSchoolModel].self, from: data)
        return result
    }
}
